# Baralho.lua
**what you can make with this:**

 - create class for mount decks
 - create *CardDatas*
 - shuffle deck
 - get cards
 - put cards
 - make decks with two inputs(max number, type number)

**Main classes:**

 - Card
 - `Deck = baralho:new()`
 - CardData
 - `CardData = {number = WhatYouWant, type = WhatYouWant}`

**Main functions**

 - `baralho:new(name = "")`
 - `baralho:make(MaxOfNumber, NumberOfTypes)`
 - `baralho:shuffle()`
 - `baralho:get(local = 1)`
 - `baralho:addCardData(CardData)`

**Main variables**

 - `baralho.name`
 - `baralho.cards` array of cardData

# Example
creating a "truco" deck (10 pseudo-number, 4 types)

[Truco](https://pt.wikipedia.org/wiki/Truco) is a popular game in Brazil what you remove the 8, 9, 10 cards of deck, you can play with 2 or 4 players


````lua
require("baralho")
basicDeck = baralho:new("basicDeck")

hand1 = {}
hand2 = {}

function love.load()
	--instantiate the basicCard.cards array with 
	--01-13 type 1 
	--13-26 type 2
	--26-39 type 3
	--39-52 type 4
	basicDeck:make(10, 4) 
	--shuffle this like 
	--before basicDeck.cards[1] = {number = 1, type = 1}
	--after basicDeck.cards[1] = {number = 2(?), type = 4(?)}
	basicDeck:shuffle()
	--call to the func
	truco()
end
````
````lua
--a distribution function
--that get the top card of deck and distribute to hand 1 and 2
function truco()
	table.insert(hand1, basicDeck:get())
	table.insert(hand1, basicDeck:get())
	table.insert(hand1, basicDeck:get())
	table.insert(hand2, basicDeck:get())
	table.insert(hand2, basicDeck:get())
	table.insert(hand2, basicDeck:get())
end
````
any question my e-mail is: nicolas.mnw@live.com
