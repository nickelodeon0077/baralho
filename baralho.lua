-------------------------------------------
--made by Nickelodeon0077, is free to use--
-------------------------------------------

baralho = {}
baralho.__index = baralho

function baralho:new (name)
  brl = {}
  setmetatable(brl, baralho)
  brl.name = name
  brl.cards = {}
  return brl
end

function baralho:make (maxNumber, cardTypes) --numero maior, maximo de tipos
  AC = 1
  CT = 1
  for i=1, maxNumber*cardTypes do

    if i > maxNumber * CT then
      CT = CT + 1
    end

    if i > 1 then
      AC = i - ((CT*10) - 10)
    else
      AC = 1
    end

    table.insert(self.cards, {number = AC, type = CT})
    i = i + 1
  end
end

function baralho:shuffle ()
  numberOfShuffle = love.math.random(11)

  local shuffle =  function (t)
    local rand = math.random
    assert(t, "table.shuffle() expected a table, got nil")
    local iterations = #t
    local j

    for i = iterations, 2, -1 do
        j = rand(i)
        t[i], t[j] = t[j], t[i]
    end
  end

  for i=1,numberOfShuffle do
    shuffle(self.cards)
  end
end

function baralho:get (loc)
  loc = loc or 1

  if #self.cards > 0 then
    toGet = self.cards[loc]
    table.remove(self.cards, loc)
    return toGet
  end
end

function baralho:addCardData (cardData)
  insert(self.cards, cardData)
end
